import { Component, OnInit } from '@angular/core';
import { Book } from '../interfaces/book';
import { BooksService } from '../services/books.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  book : Book = {
    identification_number: null,
    first_name: null,
    second_name: null,
    first_lastname: null,
    second_lastname: null
  };

  id:any;
  editing: boolean = false;
  books: Book[];
  constructor( private bookService: BooksService, private activateRoute: ActivatedRoute,private router: Router) {  
    this.id = this.activateRoute.snapshot.params['id'];
    if(this.id){
      this.editing = true;
      this.bookService.get().subscribe((data:Book[])=>{

        this.books = data;
        this.book = this.books.find((b)=>{
          return b.id == this.id;
        });
  
      }, (error)=>{
  
        console.log('error', error);
        alert('Ocurrio un error');
  
      });
    } else {
      this.editing = false;
    }
    
  }

  ngOnInit(): void {
  }

  
  saveBook(){

    if(this.editing){
        this.bookService.put(this.book).subscribe((data)=>{
          
          alert('Registro actualizado!');
          this.router.navigate(["/"]);
        }, (error)=>{
          console.log('error', error);
          alert('Ocurrio un error');
        });
    }
    else{
      this.bookService.save(this.book).subscribe((data)=>{
        console.log('error', data);
        alert('Registro exitoso!');
        this.router.navigate(["/"]);
      }, (error)=>{
        console.log('error', error);
        alert('Ocurrio un error');
      });

    }
    
  }

}
