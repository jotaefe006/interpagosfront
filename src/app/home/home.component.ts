import { Component, OnInit } from '@angular/core';
import {BooksService} from '../services/books.service';
import {Book} from '../interfaces/book'


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  books : Book[];
  constructor(private bookService: BooksService) {
    this.getBooks();
   }

  ngOnInit(): void {

  }

  getBooks(){
    this.bookService.get().subscribe((data:Book[])=>{
      this.books = data;
    }, (error)=>{
      console.log('error', error);
      alert('Ocurrio un error');
    });
  }

  delete(id){

    if(confirm('Desea eliminar este registro?')){
      this.bookService.delete(id).subscribe((data)=>{
        alert('Se eliminó con éxito!');
        this.getBooks();
        
      }, (error)=>{
        console.log('error', error);
        alert('Ocurrio un error');
      });
    }
    
  }

}
