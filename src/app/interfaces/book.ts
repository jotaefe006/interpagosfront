export interface Book {
    id?: number;
    identification_number: string;
    first_name: string;
    second_name: string;
    first_lastname: string;
    second_lastname: string;
}