import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Book} from '../interfaces/book';

@Injectable({
  providedIn: 'root'
})
export class BooksService {
  
 // API_ENDPOINT = 'http://localhost:8000';
  API_ENDPOINT = 'http://pruebav2.oo';
  constructor(private httpClient: HttpClient) {   }

  get(){
    return this.httpClient.get(this.API_ENDPOINT+ '/books');
  }

  save(book: Book){
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.httpClient.post(this.API_ENDPOINT + '/books', book, {headers:headers});

  }

  put(book){
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.httpClient.put(this.API_ENDPOINT + '/books/'+ book.id, book, {headers:headers});

  }
  delete(id){
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.httpClient.delete(this.API_ENDPOINT+ '/books/'+id, {headers:headers});

  }
}
